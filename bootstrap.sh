#!/bin/sh

PUPPETSERVER_IP="${1}"

if [ ! -d /etc/puppetlabs/puppet ]; then
   mkdir -p /etc/puppetlabs/puppet
fi

function run_puppet()
{
    if [[ "${PUPPETSERVER_IP}x" != 'x' ]] ; then
      /opt/puppetlabs/puppet/bin/puppet resource host puppet ip=$PUPPETSERVER_IP
    fi
    /opt/puppetlabs/puppet/bin/puppet agent -t
}

# This will search 'tags' for puppet_role- and assign the value to the 'ROLE' of the host
TAGS=$(curl -s http://metadata.google.internal/computeMetadata/v1/instance/tags -H "Metadata-Flavor: Google")
for TAG in $( echo $TAGS | sed -e 's/,/ /g' -e 's/"/ /g') ; do
        if [[ $TAG =~ ^puppet_role- ]]; then
                ROLE=$(echo  $TAG | sed -e 's/-/::/g' -e 's/"//g' -e 's/puppet_role//')
                break
        fi
done

# Pull additional information from metadata
ID=$(curl -s http://metadata.google.internal/computeMetadata/v1/instance/id -H "Metadata-Flavor: Google")
PROJECT=$(curl -s http://metadata.google.internal/computeMetadata/v1/project/project-id -H "Metadata-Flavor: Google")
NETWORK=$(curl -s http://metadata.google.internal/computeMetadata/v1/instance/network-interfaces/0/network -H "Metadata-Flavor: Google")

# This will check if csr_attributes.yaml exists
if [[ ! -e "/etc/puppetlabs/puppet/csr_attributes.yaml" ]] ; then

  # This checks to see if a role was assigned
  if [[ "$ROLE" != '' ]] ; then
  
cat > /etc/puppetlabs/puppet/csr_attributes.yaml << YAML
custom_attributes:
    1.2.840.113549.1.9.7: mySuperAwesomePassword
extension_requests:
    pp_instance_id: $ID
    pp_project: $PROJECT
    pp_network: $NETWORK
    pp_role: $ROLE
YAML

  else

cat > /etc/puppetlabs/puppet/csr_attributes.yaml << YAML
custom_attributes:
    1.2.840.113549.1.9.7: mySuperAwesomePassword
extension_requests:
    pp_instance_id: $ID
    pp_project: $PROJECT
    pp_network: $NETWORK
YAML
  fi
fi

run_puppet
